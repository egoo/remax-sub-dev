import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Gallery from '@/components/Gallery'
import Gallerydetail from '@/components/Gallerydetail'
import About from '@/components/About'
import News from '@/components/News'
import Properties from '@/components/Properties'
import Contact from '@/components/Contact'
import Agent from '@/components/Agent'
import Newsdetail from '@/components/Newsdetail'
import Errors from '@/components/404'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/agents',
      name: 'Agent',
      component: Agent
    },
    {
      path: '/albums',
      name: 'Gallery',
      component: Gallery
    },
    {
      path: '/albums/:id',
      name: 'Gallerydetail',
      component: Gallerydetail
    },
    {
      path: '/news',
      name: 'News',
      component: News
    },
    {
      path: '/news/:id',
      name: 'Newsdetail',
      component: Newsdetail
    },
    {
      path: '/properties',
      name: 'Properties',
      component: Properties
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    },
    {
      path: '*',
      name: 'Errors',
      component: Errors
    }
  ]
})
